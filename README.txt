
****************************************************
Account sitter - AKA In Behalf of -- README

written by ilo: ilo at reversing dot org
****************************************************

This module provides an interface that allows registered users to have an
account sitter. A Sitter will be any other registered user allowed to log 
into the drupal site (with limitations) as the sitted user in a secure way.

To use, simply enable the module, set permissions for the roleswho can have 
sitters in their profile. Visit your user edit page, enter the user name you 
would like to be your sitter, and save. The sitter user will be able to login
using your username and his password, so there's no need for a sitted user
to give his password to the sitter user. 

This module, unlike the masquerade or devel, is user-oriented. It's, devel
or masquerading module are intended to be used by site admins or developers
to switch between users. This module gives the user the feature of having
some kind of account sitter, or a way for users to operate "in behalf of".

Just as a note, this module was based in the account sitting operation of 
the web based game "travian".

INSTALLATION:

1. Put the entire 'sitter' folder in either:
      a.  Your main 'modules' folder.
      b.  The modules folder of a multisite installation.
      c.  The 'sites/all/modules' folder.

2. Enable the module at Administer -> Modules.

3. At Administer -> Access control, set the 'have sitters' permission 
   for those roles that should be able to configure a sitter at their
   profile. If a user has a sitter account (an administrator do set
   this value in his profile) but has not 'have sitters'permissions, 
   then the sitting information will be displayed as a non-editable form 
   item. 
   
4. users may see in their profile if they are sitters of other users.


CONSIDERATIONS

 Password ambiguity: when a sitted and the sitter accounts have the same 
 password, the operation should login as sitted (original) account, never 
 as sitter. This always happends due to natural workflow of the login 
 operation in drupal.

 It was included here a basic limitation in the visibility of a sitted account, 
 just to avoid secutiry risk: a sitter would not be able to change the sitted
 password, neither to modify the sitting configuration of the sitted account.


 TODO:
 - A registry of account sitting changes is to be created, to grant permissions
  to sitters in a time based manner. Currently time is being saved in the 
  database, but not being used.
 - Create a configuration page. At Administer -> User Settings, admin should be 
  able to set preference for displaying or hidding the "Sitting options" profile views.  
 - Create an access control system, so the user may define the way a sitted account 
  may do actions in the system, for example view or edit account settings, 
  post comments and so..
 - Improve the "in behalf of" usage of the module, with options like attach the
  "in behalf of user XXX" to comments or nodes, including private messages and so.

 
